import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  public token: string = 'BQA6EuCaDpNifismaT06ss5IOspL-6U7JD7cSFmn4-w9_go4hlXrQFFoPpLeW1l1-WSEl90jjDw8CHQx5IQ';

  constructor(private http: HttpClient) {



  }
  API_BASE: string = 'https://api.spotify.com/v1/';


  getQuery(queryString: string) {

    const headers = new HttpHeaders({
      'Authorization' : 'Bearer ' + this.token
    });

    return this.http.get(this.API_BASE + queryString, {headers: headers});
  }


  getNewReleases() {

    return this.getQuery('browse/new-releases')
    .pipe( map( (data: any) => {
          return data.albums.items;
      }
    ));
  }

  getAlbumsBySearch(searchString: string) {

    return this.getQuery('search?q=' + searchString + '&type=artist&limit=15')
      .pipe( map( (data: any) => {
          return data.artists.items;
      }
    ));
  }


  getArtista(id: string) {
    console.log(id);
    return this.getQuery('artists/' + id)
      .pipe( map( (data: any) => {
        return data;
          // return data.artists.items;
    }
  ));
  }


  getArtistaTopTracks(id: string) {
    console.log(id);
    return this.getQuery(`artists/${id}/top-tracks?country=US`)
      .pipe( map( (data: any) => {
        return data.tracks;
          // return data.artists.items;
    }
  ));
  }
}
