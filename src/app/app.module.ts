import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { app_routes } from './app.routes';
import { SpotifyService } from './services/spotify.service';
import { NoimagePipe } from './pipes/noimage.pipe';
import { CardsComponent } from './components/cards/cards.component';
import { ArtistCardComponent } from './components/artist-card/artist-card.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { LoadingComponent } from 'src/app/components/shared/loading/loading.component';
import { DomSeguroPipe } from './pipes/dom-seguro.pipe';
import { TrackRowComponent } from './components/track-row/track-row.component';
import { SpotyIframeComponent } from './components/spoty-iframe/spoty-iframe.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ArtistaComponent,
    NavbarComponent,
    NoimagePipe,
    CardsComponent,
    ArtistCardComponent,
    AlbumCardComponent,
    LoadingComponent,
    DomSeguroPipe,
    TrackRowComponent,
    SpotyIframeComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(app_routes, {useHash : true}),
    HttpClientModule
  ],
  providers: [SpotifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
