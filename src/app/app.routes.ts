
import { HomeComponent } from './components/home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { ArtistaComponent } from './components/artista/artista.component';
import { SearchComponent } from './components/search/search.component';


export const app_routes: Routes = [
  { path: 'home', component: HomeComponent } ,
  { path: 'artist/:id', component: ArtistaComponent } ,
  { path: 'search', component: SearchComponent } ,
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

