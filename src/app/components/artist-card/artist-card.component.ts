import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-artist-card',
  templateUrl: './artist-card.component.html',
  styleUrls: ['./artist-card.component.css']
})
export class ArtistCardComponent implements OnInit {

  @Input() artist: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  verArtista(id: number) {
    this.router.navigate(['/artist', id]);
  }

}
