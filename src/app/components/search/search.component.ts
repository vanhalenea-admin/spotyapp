import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private _spotifyService: SpotifyService) { }
  artists: any [] = [];
loading: boolean = false;

  ngOnInit() {
  }

  onSearchKeyUp(searchText: string) {
  if (searchText === '') {
    this.artists = [];
  } else {
      this.loading = true;
      this._spotifyService.getAlbumsBySearch(searchText).subscribe( (data: any) => {
      this.artists = data;
      this.loading = false;
    });
  }
  }

}
