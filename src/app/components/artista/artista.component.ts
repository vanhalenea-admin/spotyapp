import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent implements OnInit {

  artista: any;
  topTracks: any[] = [{}];
  loading: boolean = false;

  constructor(private activeRoute: ActivatedRoute, private _spotifyService: SpotifyService ) {
    this.activeRoute.params.subscribe(params => {
      // let variable = params['id'];
      this.getArtista(params['id']);
      this.getTopTracks(params['id']);
    });
  }

  ngOnInit() {
  }

  getArtista(artistId: string) {
    this.loading = true;
   this._spotifyService.getArtista(artistId).subscribe( (data: any) => {
      this.artista = data;
      this.loading = false;
    });

  }

  getTopTracks(artistId: string) {
    this._spotifyService.getArtistaTopTracks(artistId).subscribe( (data: any) => {
      this.topTracks = data;
      console.log(data);
    });
  }


}
