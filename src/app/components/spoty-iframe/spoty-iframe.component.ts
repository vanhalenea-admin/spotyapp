import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-spoty-iframe',
  templateUrl: './spoty-iframe.component.html',
  styleUrls: ['./spoty-iframe.component.css']
})
export class SpotyIframeComponent implements OnInit {

  @Input() track: any;
  safeUrl: SafeResourceUrl;
  constructor(private domSanitizer: DomSanitizer) { }

  ngOnInit() {
    this.safeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(`https://open.spotify.com/embed/${this.track.type}/${this.track.id}`);
  }

}
