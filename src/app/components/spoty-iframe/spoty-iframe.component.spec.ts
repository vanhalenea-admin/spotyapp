import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpotyIframeComponent } from './spoty-iframe.component';

describe('SpotyIframeComponent', () => {
  let component: SpotyIframeComponent;
  let fixture: ComponentFixture<SpotyIframeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpotyIframeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpotyIframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
