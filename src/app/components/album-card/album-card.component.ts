import { Component, OnInit, Input } from '@angular/core';
import { Router} from '@angular/router';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.css']
})
export class AlbumCardComponent implements OnInit {
  @Input() album: any;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  verArtista(album: any) {
    let id = 0;

    if (album.type === 'album') {
        id = album.artists[0].id;
    } else {
      id = album.id;
    }
    this.router.navigate(['/artist', id]);
  }

}
