import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-track-row',
  templateUrl: './track-row.component.html',
  styleUrls: ['./track-row.component.css']
})
export class TrackRowComponent implements OnInit {
@Input() track: any;
safeUrl: SafeResourceUrl;

  constructor(private domSanitizer: DomSanitizer) {



  }

  ngOnInit() {
    console.log(this.track);
    this.safeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(`https://open.spotify.com/embed/${this.track.type}/${this.track.id}`);
  }

}
