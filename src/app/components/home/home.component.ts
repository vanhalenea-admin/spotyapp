import { SpotifyService } from './../../services/spotify.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  albums: any [] = [];
  loading: boolean = false;

  constructor(private _spotifyService: SpotifyService) { }

  ngOnInit() {
    this.loading = true;
    this._spotifyService.getNewReleases().subscribe( (data: any) => {
      this.albums = data;
      this.loading = false;
    });
  }

}
